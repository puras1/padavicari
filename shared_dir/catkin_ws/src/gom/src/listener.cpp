#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/passthrough.h>

#include <tf/transform_listener.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr tf_values(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_values(new pcl::PointCloud<pcl::PointXYZ>);

int cell_occupancy_counter[12][8] = {0};

void callback(const sensor_msgs::PointCloud2ConstPtr &pCloud)
{
    // new cloud formation
    pcl::fromROSMsg(*pCloud, *cloud);
}

void grid_occupancy_calculate(pcl::PointCloud<pcl::PointXYZ>::Ptr translated_values)
{

    pcl::PassThrough<pcl::PointXYZ> pass;

    pass.setInputCloud(translated_values);

    pass.setFilterFieldName("x");
    pass.setFilterLimits(-6.0, 6.0);
    pass.filter(*filtered_values);  
    pass.setInputCloud(filtered_values);

    pass.setFilterFieldName("y");
    pass.setFilterLimits(-4.0, 4.0);
    pass.filter(*filtered_values);
    pass.setInputCloud(filtered_values);

    pass.setFilterFieldName("z");
    pass.setFilterLimits(-10, 0.3);
    pass.filter(*filtered_values);  
}

void grid_cell_calculate(pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_values)
{
    for (int i = 0; i < 12; i++)
    {
        for (int j = 0; j < 8; j++)
        {
          cell_occupancy_counter[i][j] = 0;

        }
    }
    float x = 0;
    float y = 0;
    int xx = 0;
    int yy = 0;

    for (int i = 0; i < filtered_values->points.size(); i++)
    {
        x = filtered_values->points[i].x + 6;
        xx = int(floor(x));
        y = filtered_values->points[i].y + 4;
        yy = int(floor(y));
        cell_occupancy_counter[xx][yy]++;
    }
        for (int i = 0; i < 12; i++)
    {
        for (int j = 0; j < 8; j++)
        {
         //std::cout << cell_occupancy_counter[i][j] <<std::endl;

        }
    }
    
}

int main(int argc, char **argv)
{
    /**

* The ros::init() function needs to see argc and argv so that it can perform
* any ROS arguments and name remapping that were provided at the command line.
* For programmatic remappings you can use a different version of init() which takes
* remappings directly, but for most command-line programs, passing argc and argv is
* the easiest way to do it. The third argument to init() is the name of the node.
*
* You must call one of the versions of ros::init() before using any other
* part of the ROS system.
*/

    ros::init(argc, argv, "listener");

    /**
* NodeHandle is the main access point to communications with the ROS system.
* The first NodeHandle constructed will fully initialize this node, and the la
* NodeHandle destructed will close down the nodfiltered_values
    **/

    ros::NodeHandle n;

    /**
* The subscribe() call is how you tell ROS that you want to receive messages
* on a given topic. This invokes a call to the ROS
* master node, which keeps a registry of who is publishing and who
* is subscribing. Messages are passed to a callback function, here
* called chatterCallback. subscribe() returns a Subscriber object that you
* must hold on to until you want to unsubscribe. When all copies of the Subscriber
* object go out of scope, this callback will automatically be unsubscribed fro
* this topic.
*
* The second parameter to the subscribe() function is the size of the message
* queue. If messages are arriving faster than they are being processed, this
* is the number of messages that will be buffered up before beginning to throw
* away the oldest ones.
*/

    ros::Subscriber sub = n.subscribe("points_raw", 1000, callback);
    ros::Publisher pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);

    visualization_msgs::Marker marker;
    marker.header.frame_id = "/base_link";
    marker.header.stamp = ros::Time::now();
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.id =0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    tf::TransformListener listener;
    tf::StampedTransform transform;

    try
    {
        listener.waitForTransform("base_link", "velodyne", ros::Time(0),
                                    ros::Duration(3.0));

        listener.lookupTransform("/base_link", "/velodyne", ros::Time(0), transform);
    }
    catch (tf::TransformException ex)
    {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
    }    
    
    while (n.ok())
    {
        pcl_ros::transformPointCloud(*cloud, *tf_values, transform);
        grid_occupancy_calculate(tf_values);
        grid_cell_calculate(filtered_values);
        for(int m = 0; m<12;m++)
        {
            for(int n=0; n<8;n++)
            {
                if(cell_occupancy_counter[m][n] > 10 && cell_occupancy_counter[m][n]<= 150)
                {
                    marker.pose.position.x = m - 5.5;
                    marker.pose.position.y = n - 3.5;    
                    marker.pose.position.z = 0;
                    marker.scale.x = 1;
                    marker.scale.y = 1;
                    marker.scale.z = 1;
                    marker.color.a = 1.0;
                    marker.color.r = 0.0;
                    marker.color.g = 1.0;
                    marker.color.b = 0.0;
                    marker.id ++;
                    pub.publish(marker);

                }
                else if(cell_occupancy_counter[m][n] > 150 && cell_occupancy_counter[m][n]<= 300)
                {
                    marker.pose.position.x = m - 5.5;
                    marker.pose.position.y = n - 3.5;    
                    marker.pose.position.z = 0;
                    marker.scale.x = 1;
                    marker.scale.y = 1;
                    marker.scale.z = 0;
                    marker.color.a = 1.0;
                    marker.color.r = 1.0;
                    marker.color.g = 1.0;
                    marker.color.b = 0.0;
                    marker.id ++;
                    pub.publish(marker);
                }
                else if(cell_occupancy_counter[m][n] > 300)
                {
                    marker.pose.position.x = m - 5.5;
                    marker.pose.position.y = n - 3.5;    
                    marker.pose.position.z = 0;
                    marker.scale.x = 1;
                    marker.scale.y = 1; 
                    marker.scale.z = 1;
                    marker.color.a = 1.0; 
                    marker.color.r = 1.0;
                    marker.color.g = 0.0;
                    marker.color.b = 0.0;
                    marker.id ++;
                    pub.publish(marker);
                }
            }
        }
        marker.id = 0;
        /**
        * ros::spin() will enter a loop, pumping callbacks. With this version, all
        * callbacks will be called from within this thread (the main one). ros::spin
        * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
        */
        ros::spinOnce();
    }

    return 0;
}